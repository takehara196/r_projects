library(xts)
library(RCurl)
url <- getURL("http://ichart.finance.yahoo.com/table.csv?s=SPY")
data <- read.csv(text = url)
data <- data[1:100,c(1,2,3,4,5)]
TodaysOpen <- data[1,2]
TodaysHigh <- data[1,3]
TodaysLow <- data[1,4]
TodaysClose <- data[1,5]
data.xts<-as.xts(read.zoo(data))


library(shiny)
library(shinydashboard)
library(dygraphs)
ui <- dashboardPage(skin="green",
                    dashboardHeader(title = "ABCD Dashboard"),
                    dashboardSidebar(
                      sidebarMenu(
                        menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard"))
                      )
                    ),
                    dashboardBody(skin="green",
                                  tabItems(
                                    tabItem(tabName = "dashboard",
                                            fluidRow(
                                              valueBoxOutput("open", width = 3),
                                              valueBoxOutput("high", width = 3),
                                              valueBoxOutput("low", width = 3),
                                              valueBoxOutput("close", width = 3)
                                            ),
                                            fluidRow(
                                              box(
                                                title = "SPDR S&P 500 ETF (SPY)", 
                                                width = 12, 
                                                solidHeader = TRUE, 
                                                dygraphOutput("dygraph_data")
                                              ) 
                                            ) 
                                    ),
                                    tabItem(tabName = "dashboard") 
                                  )
                    ) 
)
server <- function(input, output) {
  output$open <- renderValueBox({
    valueBox(
      paste0(round(TodaysOpen, digits=2)), 
      "Today’s Open", 
      icon = icon("line-chart"), 
      color = "green" 
    ) 
  })
  output$high <- renderValueBox({ 
    valueBox( 
      paste0(round(TodaysHigh, digits=2)), 
      "Today’s High", 
      icon = icon("arrow-up"), 
      color = "green" 
    ) 
  })
  output$low <- renderValueBox({
    valueBox(
      paste0(round(TodaysLow, digits=2)),
      "Today’s Low", 
      icon = icon("arrow-down"), 
      color = "green" 
    ) 
  })
  output$close <- renderValueBox({ 
    valueBox( 
      paste0(round(TodaysClose, digits=2)), 
      "Today’s Close", 
      icon = icon("balance-scale"), 
      color = "green" 
    ) 
  })
  output$dygraph_data <- renderDygraph({
    dygraph(data.xts) %>% 
      dyRangeSelector(
        height = 20, 
        strokeColor = "",
        dateWindow = c(format(as.Date(format(Sys.time(),"%Y-%m-%d"))-60), format(Sys.time(),"%Y-%m-%d"))
        ) 
  })
}
shinyApp(ui, server)