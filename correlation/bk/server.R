#
# This is the server logic of a Shiny web application. You can run the
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)

shinyServer(function(input, output, session) {

    observeEvent(input$file, {
      csv_file <- reactive(read.csv(input$file$datapath,fileEncoding="utf-8"))
      output$table <- renderTable(csv_file())
      
    })
})