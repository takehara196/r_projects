setwd("/Users/takehara/R_projects/zoku_data")

#------------------------------------------------------------------
# p.47
d2 <- read.csv("Curry.csv", fileEncoding = "cp932")
head(d2)
str(d2)

d2.s <- scale(d2[,-1])
d2.s

#------------------------------------------------------------------
# p.53
fa2 <- factanal(d2[,-1], factors = 2, rotation="none")
print(fa2$loadings, sort=T) # 因子負荷量が大きい順に並び替え

# 因子負荷量をplotしてみる(無回転)
plot(fa2$loadings[,1], fa2$loadings[,2],
     xlim=c(0,1), ylim=c(-1,1))
abline(a=0, b=0, lty=2, col=2) #この2行はx軸, y軸を書いているだけ
abline(v=0, lty=2, col=2)

#------------------------------------------------------------------
# p.55
# 因子負荷量をplotしてみる(無回転 再掲)
plot(fa2$loadings[,1], fa2$loadings[,2],
     xlim=c(0,1), ylim=c(-1,1))
abline(a=0, b=0, lty=2, col=2) #この2行はx軸, y軸を書いているだけ
abline(v=0, lty=2, col=2)

# factanal関数による因子分析(varimax回転)
fa2.vari <- factanal(d2[,-1], factors = 2, rotation="varimax")
print(fa2.vari$loadings, sort=T)

# 因子負荷量をplotしてみる(回転後)
plot(fa2.vari$loadings[,1], fa2.vari$loadings[,2],
     xlim=c(0,1),ylim=c(-1,1))
abline(a=0, b=0, lty=2, col=2)
abline(v=0, lty=2, col=2)

# 詳細
print(fa2, sort=T)

#------------------------------------------------------------------
# p.60 データは新卒
d <- read.csv("Shinsotsu.csv", fileEncoding = "cp932")
fa3 <- factanal(d[,-1], factors = 3,
                rotation="promax", scores="Bartlett")
print(fa3, sort=T)

# p.62 寄与率と独自性の関係
mean(fa3$uniquenesses)+0.843 # 0.843は累積寄与率
round(mean(fa3$uniquenesses)+0.843)
