library(shiny)


ui <- shinyUI(
  fluidPage(
    tags$head(tags$link(rel = "stylesheet", href = "styles.css", type = "text/css"),
              tags$script(src = "getdata3.js")),
    sidebarLayout(
      sidebarPanel(
        h2("赤枠にデータをドロップ"),
        div(id="drop-area", ondragover = "f1(event)", ondrop = "f2(event)"),
        h2("プロットするデータを選択"),
        htmlOutput("colname1"),
        htmlOutput("colname2"),
        actionButton("submit", "プロット")
      ),
      mainPanel(
        tabsetPanel(type = "tabs",
                    tabPanel("Table", tableOutput('table')),
                    tabPanel("Plot", plotOutput("plot"))
        )
      )
    )
  )
)


server <- shinyServer(function(input, output, session) {
  observeEvent(input$mydata, {
    
    name = names(input$mydata)
    csv_file = reactive(read.csv(text=input$mydata[[name]]))
    output$table = renderTable(csv_file())
    
    output$colname1 = renderUI({ 
      selectInput("x", "x軸方向", colnames(csv_file()))
    })
    output$colname2 = renderUI({ 
      selectInput("y", "y軸方向", colnames(csv_file()))
    })
  })
  
  observeEvent(input$submit, {
    name = names(input$mydata)
    csv_file = reactive(read.csv(text=input$mydata[[name]]))
    
    x = csv_file()[input$x]
    y = csv_file()[input$y]
    
    output$plot = renderPlot({
      plot(x[,1], y[,1])
    })
  })
})

shinyApp(ui = ui, server = server)