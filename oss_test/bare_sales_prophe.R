library(prophet)

# # Load a script
# source("/Users/takehara/R_projects/oss_test/clear_func.R")
# # Check
# print( "Siren's song here" )
# clearConsole()


for (id in 188:190) {
  print(id)
  #tryCatch({

    # エラーや警告が発生したときに例外処理を行いたいコード
    id <- formatC(id,width=6,flag="0")
    read_path <- paste("/Users/takehara/PycharmProjects/CS_analitics/out/baremetal_sales_SL",id,".csv",sep="")
    df <- read.csv(read_path)
    m <- prophet(df, seasonality.mode = 'multiplicative')
    future <- make_future_dataframe(m, periods = 365)
    fcst <- predict(m, future)
    plot(m, fcst)
    
    future <- make_future_dataframe(m, periods = 12, freq = 'month')
    fcst <- predict(m, future)
    plot(m, fcst)
    
    write_path <- paste('/Users/takehara/PycharmProjects/CS_analitics/out/img_out/SL',id,'.png',sep="")
    
    png(write_path, width = 640, height = 480)
    plot(m, fcst)
    dev.off()
    
  # },
  # warning = function(e) {  # e には警告文が保存されている
  #   message("該当IDなし")
  #   message(e)
  #   
  # }
  # )
}

