# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# BiocManager::install(version = "3.10")

### 準備 ###
library(ropls)
# roplsパッケージに格納されているsacurineデータの読み込み
data(sacurine)
# 解析対象となる列の読み込み
working_df <- data.frame(sacurine$sampleMetadata, sacurine$dataMatrix)
working_df



### データの可視化 ###
library(FactoMineR)
library(factoextra)
pca_res <- PCA(working_df[, 4:112], # データ読み込み
               graph = FALSE,
               ncp = 10) # 結果に保存する主成分の数


# 主成分分析の結果であるpca_resの可視化
fviz_pca_var(pca_res, # 上記で作成・保存したPCAの結果
             axes = c(1,2), # 表示したい成分の指定
             col.var="contrib", # 寄与度を色で表記
             repel = TRUE, # ラベルの重なりをなるべく回避
             labelsize = 1 # ラベルのフォントサイズ
             )


# 各成分の寄与率
fviz_screeplot(pca_res, # 上記で作成・保存したPCAの結果
               addlabels = TRUE, # ラベルを表示するかどうか
               ylim = c(0,20)) #　横軸のした上限
# 第一主成分の寄与率は14.9%, 第二主成分の寄与率は10.3%
# 第一主成分と第二主成分の寄与率との合計は約25%
# 第5主成分まで含めても40%程度


# 例として第一主成分に寄与する因子5つを表示する
# 赤の破線は寄与率の平均
fviz_contrib(pca_res, # 上記で作成・保存したPCAの結果)
             choice="var", #変数を指定
             axes = 1, # 変数を見たい成分の指定
             top=5 # 上位いくつめの成分まで表示するか
             )


# 例として第一主成分，第二主成分に対して，性差(working_df$gender)について可視化する
# 色分けしたいグループを指定
fviz_pca_ind(pca_res, # 上記で作成・保存したPCAの結果
             habillage = working_df$gender, # 色分けしたいグループの指定
             geom = "point", # 点のみの表示
             poitsize = 3, # 点の大きさ指定
             repel = TRUE,  # ラベル保行きの重なりをなるべく避ける
             addEllipses = TRUE, # 円の表示をするかどうか
             ellipse.level = 0.95 # 楕円の領域
)


# 上記可視化では楕円がほぼオーバーラップしており，
# この2つの主成分では男女2群の判別がうまくいっていない
# 解消方法: axesの値を変更することで適度に分離された重なりの少ない主成分の組み合わせを探索する
fviz_pca_ind(pca_res, 
             axes = c(5,9), #表示したい成分の指定
             habillage = working_df$gender, # 色分けしたいグループの指定
             geom = "point", #点のみの表示
             pointsize = 3, # 点の大きさ指定
             repal = TRUE, # ラベル表記の重なりをなるべく避ける
             addEllipses = TRUE # 円の表示をするかどうか
             )

# BiocManager::install("ComplexHeatmap")

# claster_working_df <- scale(working_df[,4:112], # データの標準化
claster_working_df <- scale(working_df[,4:20], # データの標準化
                            center = TRUE,
                            scale = TRUE)

### ヒートマップ ###
heatmap(claster_working_df, # 標準化hしたデータの指定
        row_names_gp =  grid::gpar(fontsize = 4), # x軸のフォントサイズ
        row_names_max_width = unit(7, "cm"), # x軸の高さ
        column_names_gp =  grid::gpar(fontsize = 6), # y軸のフォントサイズ
        column_names_max_height = unit(15, "cm"), # y軸の高さ
        row_title = "ID",
        column_title = "Metabolome", # x軸の名前
        split = working_df$gender, # 男女で分割
        col = heat.colors(256)    # 色彩の指定
        )

### 散布図行列 ###
# install.packages("GGallay")
library(GGally)
ggpairs(data = working_df[, 1:8], # 一部のデータを指定
        mapping = aes(color = gender))



### 検定 ###
res_t_test <- apply(working_df[, 4:112], 2,
                    function(x) t.test(x ~ working_df$gender)$p.value)

bonferoni_res <- p.adjust(res_t_test,
                      method = "bonferroni", # ボンフェローニ法でp値を補正
                      n = ncol(working_df[, 4:112])) # 補正に使う変数の数指定

bonferoni_res <- data.frame(bonferoni_res) # 結果を見やすくするためデータフレームに変形

subset(bonferoni_res, bonferoni_res < 0.05) # p値 < 0.05 の因子を抽出

# 表示を簡略化するため一部のみ解析
fdr_res <- p.adjust(res_t_test,
                    method = "fdr", # false-discovory rateでp値を補正
                    n = ncol(working_df[, 4:112])) # 補正に使う変数の数指定

fdr_res <- data.frame(fdr_res) # 結果を見やすくするためデータフレームに変形
subset(fdr_res, fdr_res < 0.05)


### 機械学習に判別分析 ###
library(ropls)

data(sacurine)
set.seed(71)
opls_res <- opls(sacurine$dataMatrix, 
                 sacurine$sampleMetadata[, "gender"],
                 predI = 1,
                 orthoI = NA,
                 permI = 500,
                 crossvalI = 7,
                 scaleC = "standard",
                 printL = FALSE,
                 plotL = FALSE
                 )

# 結果の表示
opls_res
