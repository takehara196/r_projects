# setwd("/Users/takehara/R_projects/zoku_data")

# データの確認
d <- read.csv("keiyaku.csv", fileEncoding = "cp932")
d

# 散布図
par(family = "Osaka")
plot(契約結果 ~ 担当者年齢, data=d)
plot(契約結果 ~ as.numeric(訪問時刻), data=d)
plot(契約結果 ~ jitter(as.numeric(訪問時刻)), data=d) # 少し誤差を加えている


# GLMでロジスティック
library(MASS)
logi <- glm(契約結果 ~ 訪問時刻 + 担当者年齢, data=d, family = "binomial")
logi

# 他のモデルを試してみる
logi2 <- glm(契約結果 ~ 担当者年齢, data=d, family = "binomial")
logi2

summary(logi)

new.d <- data.frame(訪問時刻 = c("午後"), 担当者年齢=(34))
predict(logi, new.d, type="response")


# p.18
# オッズ
exp(logi$coefficients)[-1] # [-1]はインターセプトを抜いている
# 訪問時刻午前   担当者年齢 
# 0.0351306    1.2944872 
# 午後を午前にすると0.03倍になる
# 担当者年齢を1つあげると1.3倍になる


# p.21
# データをみる
Titanic
d.t <- data.frame(Titanic)
d.t

# rawデータへの変換
d.raw<-data.frame(
  Class=rep(d.t[,1],d.t[,5]),
  Sex=rep(d.t[,2],d.t[,5]),
  Age=rep(d.t[,3],d.t[,5]),
  Survived=rep(d.t[,4],d.t[,5])
)

head(d.raw, 50)
str(d.raw)

index <- sample(1:2201,1500)
train<- d.raw[index,]
test <- d.raw[-index,]
head(index)
head(train)

# ロジスティック回帰
lrg.tit <- glm(Survived ~ Class, data=train, family = binomial)
lrg.tit
summary(lrg.tit)

# 予測正答率
p <- predict(lrg.tit, test, type="response")
yp <- ifelse(p < 0.5, "No", "Yes")
t <- table(test$Survived)
cr <- sum(diag(t))/sum(t)
cr

# 繰り返し処理(for文を用いた)
crs <- NULL
for(i in 1:100){
  index <- sample(1:2201, 1500)
  train <- d.raw[index,]
  test <- d.raw[-index,]
  lrg.tit <- glm(Survived ~ Class, data=train, family=binomial)
  p <- predict(lrg.tit, test, type="response")
  yp <- ifelse(p<0.5, "No", "Yes")
  t <- table(test$Survived, yp)
  crs[i] <- sum(diag(t)/sum(t))
}

crs
mean(crs)
