library(shiny)
library(DT)
library(fmsb)
library(ggplot2)
library(ggrepel)

shinyServer(function(input, output, session) {
  # pokemon <- read.csv("Pokemon_r.csv")
  pokemon <- read.csv("btd_anc.csv")

  output$dt1 <- DT::renderDataTable(pokemonSubset(), selection = list(mode = 'single', selected = 1))
  output$radarPlot <- renderPlot({
    ps <- pokemonSubset()
    maxmin <- data.frame(
      hp = c(120, 1),
      attack = c(120, 1),
      defense = c(120, 1),
      spattack = c(120, 1),
      spdefense = c(120, 1),
      speed = c(120, 1))
    dat <- data.frame(
      hp = ps[1,"HP"],
      attack = ps[max(1,input$dt1_rows_selected),"Attack"],
      defense = ps[max(1,input$dt1_rows_selected),"Defense"],
      spattack = ps[max(1,input$dt1_rows_selected),"SpecialAtk"],
      spdefense = ps[max(1,input$dt1_rows_selected),"SpecialDef"],
      speed = ps[max(1,input$dt1_rows_selected),"Speed"])  
    dat <- rbind(maxmin, dat)   
    radarchart(dat, axistype = 2, 
               seg = 5, plty = 1, 
               pfcol = "#FF00004C",
               vlcex = 1.5,centerzero = TRUE, 
               title = ps[max(1,input$dt1_rows_selected),"Name"])
  })
  output$scatterPlot <- renderPlot({
    ps <- pokemonSubset()
    ggplot() + 
      geom_point(data = ps, aes(x = Speed, y = Total, color = Type1)) +
      geom_point(data = ps[max(1,input$dt1_rows_selected),], aes(x = Speed, y = Total), color = 'black', size = 3) + 
      geom_label_repel(data = ps[max(1,input$dt1_rows_selected),], aes(x = Speed, y = Total), label = ps[max(1,input$dt1_rows_selected),"Name"])
  })
})
