# setwd("/Users/takehara/Desktop/zoku_data/")

d <- read.csv("new.salary.csv")
head(d)
str(d)
summary(d)

par(family= "HiraKakuProN-W3")
plot(d)


# フォント指定
par(family="Osaka")
# 散布図の作成
plot(d[,-1])
cor(d[,-1])


# 重回帰式
LM.all <- lm(給与 ~ ., data=d[,-1]) # .で全部
LM.all
summary(LM.all)

# p.7
LM.abs.out <- lm(給与 ~ 勤続年数 + 仕事量 + 特殊免許, data=d[,-1])
LM.abs.out
summary(LM.abs.out)

library(MASS)
LM.aic <- stepAIC(LM.all)
LM.aic
summary(LM.aic)

# 多重共線性の検討
cor(d[,-1])

# 2*2で表示する
par(mfrow=c(2,2))
plot(LM.aic)


# 一列目の削除(0があるとエラーしてしまうため)
pressure2 <- pressure[-1,]

par(mfrow = c(1,2))
plot(pressure ~ temperature, pressure2, pch=19)
plot(log(pressure) ~ log(temperature), pressure2, pch=19)

# 古典的モデル
ans1 <- lm(log(pressure) ~ log(temperature), pressure2)
abline(ans1)
summary(ans1)

