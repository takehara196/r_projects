# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# BiocManager::install(version = "3.10")

### 準備 ###
# library(ropls)
# # roplsパッケージに格納されているsacurineデータの読み込み
# data(sacurine)
# # 解析対象となる列の読み込み
# working_df <- data.frame(sacurine$sampleMetadata, sacurine$dataMatrix)
# working_df

working <- read.table("/Users/takehara/R_projects/Survey/input/network.csv",
                      sep = ",",                #カンマ区切りのファイル
                      header = TRUE,            #1行目はヘッダー(列名)
                      stringsAsFactors = FALSE, #文字列を文字列型で取込む
                      fileEncoding="UTF-8")     #文字コードはUTF-8


working_df <-  data.frame(working)



### データの可視化 ###
library(FactoMineR)
library(factoextra)
# pca_res <- PCA(working_df[,4:112], # データ読み込み
#                graph = FALSE,
#                ncp = 10) # 結果に保存する主成分の数

pca_res <- PCA(working_df[,1:4],
               graph = FALSE,
               ncp = 10)

# 主成分分析の結果であるpca_resの可視化
fviz_pca_var(pca_res, # 上記で作成・保存したPCAの結果
             axes = c(1,2), # 表示したい成分の指定
             col.var="contrib", # 寄与度を色で表記
             repel = TRUE, # ラベルの重なりをなるべく回避
             labelsize = 1 # ラベルのフォントサイズ
)


# 各成分の寄与率
fviz_screeplot(pca_res, # 上記で作成・保存したPCAの結果
               addlabels = TRUE, # ラベルを表示するかどうか
               ylim = c(0,20)) #　横軸のした上限

par(family = "Osaka")
par(family = "HiraKakuProN-W3")
# 例として第一主成分に寄与する因子5つを表示する
# 赤の破線は寄与率の平均
fviz_contrib(pca_res, # 上記で作成・保存したPCAの結果)
             choice="var", #変数を指定
             axes = 1, # 変数を見たい成分の指定
             top=5 # 上位いくつめの成分まで表示するか
)


# 例として第一主成分，第二主成分に対して，性差(working_df$gender)について可視化する
# 色分けしたいグループを指定
fviz_pca_ind(pca_res, # 上記で作成・保存したPCAの結果
             habillage = working_df$gender, # 色分けしたいグループの指定
             geom = "point", # 点のみの表示
             poitsize = 3, # 点の大きさ指定
             repel = TRUE,  # ラベル保行きの重なりをなるべく避ける
             addEllipses = TRUE, # 円の表示をするかどうか
             ellipse.level = 0.95 # 楕円の領域
)


# 上記可視化では楕円がほぼオーバーラップしており，この2つの主成分では男女2

