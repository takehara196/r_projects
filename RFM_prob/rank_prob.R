# RFM分析の結果から,分散分析,ロジスティック回帰分析を行い顧客ごとの反応確率を出力する
# 入力: RFM.txt
# 出力: rank_prob.csv

library(ggplot2)

# データの読み込み
#rfm <- read.csv(file="./bare_sales_output_normalize_2.csv")
rfm <- read.csv(file="./RFM.txt")
# データの確認
dim(rfm)
summary(rfm)
# 数量データR, F, Mのヒストグラムをみておく
par(mfrow=c(1,3)) # 横に3つの図を並べる
hist(rfm$F)
hist(rfm$M)
hist(rfm$R)

# 5分位分析（デシル分析の簡単版）
# 変数R,F,Mはそれぞれ
# R:「直近来店時からの経過日数」
# F:「累積来店回数」
# M:「累積購買金額」
# CRMのための基本三因子として活用されるようになってきている
# まずはシンプルなデータ活用法である10分位分析（デシル分析）
# 但しこの資料ではレコード数が2000人と `比較的少数`のため5分位分析を行ってみる

# 四分位範囲
qt <- quantile(rfm$F, probs = c(0,0.2,0.4,0.6,0.8,1))
# カテゴリー化: cut()
rankF <- cut(rfm$F, breaks = qt, labels = c("F1", "F2", "F3", "F4", "F5"), include.lowest = TRUE)

class(rankF) # 変数の型を調べる

length(rankF) # 含まれている値の数を確認する

rfm$F[1:20] # 元の数量の値
#rfm$F[1:12]

table(rankF) # 分布: 理屈としては(n/カテゴリー数)ずつのクラスになるはず

# 同じ値は同じクラスに入るため,値が小数ではなく,整数で値が重複する人が多い場合は,
# 正確に(n/カテゴリー数)人ずつのクラスにはならない。とはいえ,分析に困ることはない。
qt <- quantile(rfm$M, probs = c(0,0.2,0.4,0.6,0.8,1))
# print(qt)
rankM <- cut(rfm$M, breaks = qt, labels = c("M1", "M2", "M3", "M4", "M5"), include.lowest = TRUE)
length(rankM)
table(rankM)

# 「直近上昇日からの経過日数」は工夫が必要である。
# 経過日数は短いほどよいから,最も短いクラスを高評価の“R5”という名前とし,
# 経過日数が増えるにしたがって“R5”->“R1”とクラス名をつける。
#qt <- quantile(rfm$R, probs = c(0.2,0.4,0.6,0.8,1))
qt <- quantile(rfm$R, probs = c(0,0.2,0.4,0.6,0.8,1))
rankR <- cut(rfm$R, breaks = qt, labels = c("R5","R4", "R3", "R2", "R1"), include.lowest = TRUE)
length(rankR)

### 箱ひげ図の作成 ###
# 累積来店回数(F)の箱ひげ図をrankFで層別化
# クラスが最下位の“F1”からより上位のクラスへ移るごとに来店回数が増えていることを再確認できる
ggplot(rfm, aes(x=rankF, y=F)) + geom_boxplot()
# 累積購買金額(M),直近来店時からの経過日数(R)も同様
ggplot(rfm, aes(x=rankM, y=M)) + geom_boxplot()
# 経過日数(R)についても箱ひげ図が理屈にかなっているかをチェック
# 直近来店時からの経過日数(R)については最も経過日数が少ないクラスを高く評価して“R5”としている点に注意
ggplot(rfm, aes(x=rankR, y=R)) + geom_boxplot()

# データフレームに新しくデータを追加する。
rfm <- data.frame(rfm, rankF, rankM, rankR) # 同じ長さ(行数)の表やデータを一つにまとめる
class(rfm) # 型確認

head(rfm) # 最初の数行を確認

### 来店回数と購買金額で優良顧客を選び出す ###
# 累積来店回数の上位クラス“F5”, “F4”だけを取り出す。
goodF <- subset(rfm, subset = (rankF=="F5"|rankF=="F4")) # 行や列を取り出すときはエクセルよりRのほうが遥かに効率的
dim(goodF)

# 上記,更に累積購買金額で絞り込む。
goodFM <- subset(goodF, subset = (rankM=="M5"|rankM=="M4")) # 来店回数（F)で上位層を取り出した後、購買金額（M)の上位層を抽出する
dim(goodFM)

head(goodFM,10)

# 2回の絞り込みで顧客を抽出した。どのような顧客が選び出されたのか要約する。
summary(goodFM)

# 当然ながら,rankFについては“F1”, “F2”, “F3”,
# また,rankMについては“M1”, “M2”, “M3”が含まれていない。
# この段階で, 前回のダイレクトメール(クーポン付き)に反応して来店した客の割合が
# 339/(118+339)=0.74となり4人の内の3人がメールに反応したことが分かる。


###################################################
### ロジステッィク回帰分析で予測モデル―全データ ###
###################################################
# データの全体を使ってダイレクトメール(クーポン付き)への反応予測モデルを作る方がよい
model1 <- glm(DM ~ F + M + R, family=binomial(link="logit"), data=rfm)
summary(model1)

exp(model1$coefficients)

rfm$prob <- model1$fitted.values
summary(rfm$prob)

ggplot(rfm, aes(x=prob)) + geom_line(stat="density")

ggplot(rfm, aes(x=prob, color=rankF)) + geom_density()

ggplot(rfm, aes(x=prob, color=rankM)) + geom_density()

ggplot(rfm, aes(x=prob, color=rankR)) + geom_density()

summary(subset(rfm, rankR == "R1"))


### ロジスティック回帰分析で予測モデル ###
sample(1:10,10)

# 2000人の顧客をテスト用の200人とトレーニング用の1800人に分けましょう。
idx <- sample(1:2000, 2000) # 1から2000までを乱順に並べる
test.id <- idx[1:200] # 最初の200個を保存
train.id <- idx[201:2000] #　残りの1800個の数字を保存
test <- rfm[test.id, ] # データセット"rfm"からtest.idで保存された行番号をとりだす
train <- rfm[train.id, ] # 残りの1800行をとりだす

dim(train)

summary(train)

dim(test)

summary(test)

model2 <- glm(DM ~ F + M + R, family=binomial(link="logit"), data=train)
summary(model2)

exp(model2$coefficients)

pred <- predict(model2, newdatda=train, type="response")
yn.yosoku <- ifelse(pred > 0.5, "Y", "N")
yn.yosoku <- as.factor(yn.yosoku)
tbl <- table(yn.yosoku, train$DM)
tbl

(tbl[1,1]+tbl[2,2])/(sum(tbl)) # 全体として何パーセントを正しく予測できたか　➡　この指標を「精度」といいます


tbl[2,2]/sum(tbl[2,]) # 「反応するだろう」と予測した人の何パーセントが実際に反応したか　➡　この指標を「適合率」といいます

tbl[2,2]/sum(tbl[,2]) #　実際に反応した人の何パーセントが事前に予測できていたか　➡　この指標を「再現率」といいます


pred <- predict(model2, newdata=test, type="response") # テストデータについて反応確率を求める
yn.yosoku <- ifelse(pred > 0.5, "Y", "N") # 確率が0.5を超える人は"Y"、0.5未満の人は"N"とする
yn.yosoku <- as.factor(yn.yosoku) # "Y", "N"の文字を分類のための因子（ファクター）にしておく

tbl <- table(yn.yosoku, test$DM) # テストデータに含まれる200人の実際の行動を説明変数R,F,Mから正しく予測できたかを表にする
tbl

(tbl[1,1]+tbl[2,2])/(sum(tbl)) # 全体として何パーセントを正しく予測できたか　➡　この指標を「精度」といいます

tbl[2,2]/sum(tbl[2,]) # 「反応するだろう」と予測した人の何パーセントが実際に反応したか　➡　この指標を「適合率」といいます

tbl[2,2]/sum(tbl[,2]) #　実際に反応した人の何パーセントが事前に予測できていたか　➡　この指標を「再現率」といいます
