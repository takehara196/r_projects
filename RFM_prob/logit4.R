# model <- glm(解約変数 ~ F + M + R, family=binomial(link="logit"), data=データセット名)
# summary(model)
# exp(model$coefficients)
# データセット名$prob <- model$fitted.values
# summary(データセット名$prob)
# ロジスティック回帰分析における「理論値(fitted.values)」はそれぞれの顧客が解約する確率

d <- read.csv("/Users/takehara/PycharmProjects/CS_analitics/RFM-value_ov.csv")
# d <- read.csv("/Users/takehara/PycharmProjects/CS_analitics/df_rank_rfm_count.csv")
d

model <- glm(day_diff~ R_value + F_value + M_value, family=binomial(link="logit"), data=d)
summary(model)
exp(model$coefficients)
d$prob <- model$fitted.values
d$prob
summary(d$prob)


z <- data.frame(Titanic)
z
Titanic1 <- data.frame(
  Class = rep(z[,1],z[,5]),
  Sex = rep(z[,2], z[,5]),
  Age = rep(z[,3],z[,5]), Survived=rep(z[,4],z[,5])
)

r <- glm(Survived ~ Class, data=Titanic1, family="binomial")
r$fitted.values
table(r$fitted.values)
exp(r$coefficient)[-1]

