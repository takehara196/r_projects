# データの読み込み
d <- read.csv("/Users/takehara/PycharmProjects/CS_analitics/df_rank_rfm_count.csv")
d


# データをみる
d.t <- data.frame(d)
d.t


# rawデータへの変換
d.raw <- data.frame(
  customer_rank = rep(d.t[,1], d.t[,6]),
  R_value = rep(d.t[,2], d.t[,6]),
  F_value = rep(d.t[,3], d.t[,6]),
  M_value = rep(d.t[,4], d.t[,6]),
  day_diff = rep(d.t[,5], d.t[,6])
)


d.raw

head(d.raw, 10)
str(d.raw) # 2201レコード



# トレーニングデータとテストデータに分ける
index <- sample(1:400, 120) # 非復元抽出で1:2201から一様乱数
train <- d.raw[index,]
test <- d.raw[-index,]
head(index)
head(train)


# ロジスティック回帰
lrg.tit <- glm(day_diff ~ customer_rank, data=d.raw, family="binomial")

# lrg.tit <- glm(customer_rank ~ R_value+F_value+M_value, data=train, family=binomial)
lrg.tit
summary(lrg.tit)



# 必要なカラムの抽出
columnList <- c("R_value", "F_value", "M_value", "day_diff")
d <- d[, columnList]
d
