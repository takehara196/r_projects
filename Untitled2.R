iro <- function(data,
                a,
                iro1)
{
  n <- nrow(data)
  data$cumsum <- round(cumsum(data[,3]),2)
  iro <- c()
  for(i in 1:n){
    iro[i] <- ifelse(data[i,5] < a[1],iro1[1],
                     ifelse(data[i,5] >= a[1] & data[i,5] < a[2],
                            iro1[2],iro1[3]))
  }
  list(iro=iro)
}

ABC <- function(data,
                sen,
                tyou, #x軸の幅調整
                main1)
{
  n <- nrow(data) 
  y1 <- max(data[,1])
  par(mar=c(4,4,4,4),omi=c(0.2,0.2,0.2,0.2))
  barplot(data[,1],space=0,ylim=c(0,c(y1+20)),
          xlab="商品",ylab="売上高",col=data[,6])
  #axis(1, at=1:25-0.1, labels=rownames(aa))#######
  par(new=T)
  plot(data[,5],type="l",xlab="商品",ylab="売上高",
       main=main1,col=1,lwd=sen,axes=F)
  axis(1, at=1:n+tyou ,labels=rownames(data))##
  axis(4)
  mtext("累積構成比率",side=4,line=3)
}

library("qicharts")

x <- rep(LETTERS[1:length()], c(100, 200, 150, 25,
                          21,47,11,176,338,36,199,238,263,
                          12, 36, 23, 2, 61,153,155,23,57,89,99,52))
aa <- paretochart(x)

aa$p <- round(aa[,4]/100,2)#比率単位の変更
a <- c(0.5,0.85)#区間設定
iro1 <- c(4,3,2)#色設定

aa1 <- cbind(aa,iro(aa,a,iro1))

main1 <- "売上高のABC分析"
sen <- 1
tyou <- 0.07
ABC(aa1,sen,tyou,main1)
