setwd("/Users/takehara/R_projects/zoku_data")


# p.69
# irisデータの例示
iris
iris[c(1,51,101),]

# setosa種とvirginica種を取り出す
seto <- subset(iris[1:50,], select=-Species)
virgi <- subset(iris[101:150,], select = -Species)

# 2種の平均を求める, 2で縦方向
(seto.m <- apply(seto[1:45,],2,mean))
(virgi.m <- apply(virgi[1:45,],2,mean))

# 2種の分散共分散行列を求める
(seto.v <- var(seto[1:45,]))
(virgi.v <- var(virgi[1:45,],))

# マハラノビス汎距離を計算
D1 <- mahalanobis(seto[46:50,],seto.m,seto.v)
D2 <- mahalanobis(seto[46:50,],virgi.m,virgi.v)
cbind(D1,D2)


#---p.71 第7章クラスター分析---#
# p.73

TOEIC <- read.csv("ch13cluster.csv")
TOEIC
# 標準化: 絶対やるものだと考えてよい
TOEIC.s <- scale(TOEIC[,-1])

# 距離を計算(ユークリッド)
(dis <- dist(TOEIC.s))

# クラスター分析(ウォード法)とデンドログラムの表示
hcr <- hclust(dis, method="ward.D2")
plot(hcr)

# クラスターをカットし，元のデータフレームと結合
cluster <- cutree(hcr, k=3)
table(cluster)
(result <- data.frame(TOEIC, cluster))

# クラスター平均を求めて，各クラスターを解釈
aggregate(.~cluster, result, mean)









iris.km <- kmeans(iris[,-5], centers=3, iter)
iris.km

#可視化
#install.packages("cluster")

library(cluster)
plot(pam(iris[,1:4],3), ask=TRUE)

d <- read.csv("kmeans.demo.csv")
df <- d[,-1]

clust <- kmeans(df, centers=3, iter.max=1)$cluster
result <- data.frame(df, clust)
plot(df$x1, df$y1, col=clust, pch=clust)

clust <- kmeans(df, centers=3, iter.max = 10)$cluster
result <- data.frame(df, clust)
plot(df$x1, df$y1, col=clust, pch=clust)

# install.packages("mclust")
library(mclust)
irisBIC <- mclustBIC(iris[,-5],)
irisBIC
plot(irisBIC)

irisBIC <- mclustBIC(iris[,-5], G=seq(from=1, to=9, by=1),
                     modelNames = c("EII", "EEI", "EEE"))

irisBIC
plot(irisBIC)

