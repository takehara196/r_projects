# setwd("/Users/takehara/R_projects/zoku_data")

pressure2 <- pressure[-1,]

# 散布図の確認
par(mfrow = c(1,2))
plot(pressure ~ temperature, pressure2, pch=19)
plot(log(pressure) ~ log(temperature), pressure2, pch=19)

# 古典的モデル
ans1 <- lm(log(pressure) ~ log(temperature), pressure2)
abline(ans1)
summary(ans1)


# 表示形式変更
par(mfrow = c(1,1))
# p.10
# 係数の算出
a1 <- exp(coefficients(ans1)[1])
b1 <- coefficients(ans1)[2]

ans2 <- nls(pressure ~ a*temperature^b, pressure2,
            start = list(a=a1, b=b1),
            control = nls.control(maxiter = 70))

summary(ans2)

# 図にして確認
plot(pressure~temperature, data=pressure2, pch=19)
lines(pressure2$temperature,predict(ans2,newdata=pressure2))
lines(pressure2$temperature,exp(predict(ans1,newdata = pressure2)))

# R^2をみてはいけない。なぜなら直線への当てはまりだから。残渣二乗和をみる。
# ans1
sum((pressure2$pressure - exp(predict(ans1, newdata=pressure2)))^2)
# ans2
sum((pressure2$pressure - predict(ans2, newdata=pressure2))^2)


# p.11
# 古典的モデル
plot(log(pressure) ~ temperature, pressure2, pch=19)
ans3 <- lm(log(pressure) ~ temperature, pressure2)
abline(ans3)
summary(ans3) # 古典的当てはめ詳細

# 係数の算出(nlsの初期値として用いる)
a2 <- exp(coefficients(ans3)[1])
b2 <- exp(coefficients(ans3)[2])

# 非線形最小二乗法
ans4 <- nls(pressure ~ a*b^temperature, data=pressure2,
            start=list(a=a2,b=b2))
summary(ans4) #nls関係による当てはめ

# 理論式
ans5 <- nls(pressure ~ exp(a/(temperature+273)+b),pressure2,start=list(a=1,b=1))
summary(ans5)

# 図にして確認
plot(pressure~temperature,data=pressure2,pch=19)
lines(pressure2$temperature,exp(predict(ans3,newdata=pressure2)))
lines(pressure2$temperature,predict(ans4,newdata=pressure2))
lines(pressure2$temperature,predict(ans5,newdata=pressure2),col="red")

