#ライブラリの読み込み
library("ggtern")

###データ例の作成#####
TastData <- data.frame(X = runif(10),
                       Y = runif(10),
                       Z = runif(10))
########

#三角ダイアグラムを作成:ggternコマンド
TerDia <- ggtern(data = TastData, aes(X, Y, Z))
#内容確認
TerDia

###作成した三角ダイアグラムの体裁を調整###
#シンボルをプロット
TerDiaPoint <- TerDia + geom_point(col = "blue")
#内容確認
TerDiaPoint

#図を回転:theme_rotateコマンド
TerDiaPoint + theme_rotate(90)

#外周に矢印を追加:theme_showarrowsコマンド
TerDiaPoint + theme_showarrows()

#データの分布をグラデーションで表示:stat_density_ternコマンド
ggtern(data = TastData, aes(X, Y, Z)) +
  stat_density_tern(geom = "polygon",
                    aes(fill=..level..),
                    base = "ilr")

#シンボルから各辺へ線を引く:geom_Tmark,_Lmark,_Rmark,_crosshair_ternコマンド
TmTerDiaPoint <- TerDiaPoint + geom_Tmark(col = "#4b61ba") + labs(title = "geom Tmark")
LmTerDiaPoint <- TerDiaPoint + geom_Lmark(col = "#a87963") + labs(title = "geom Lmark")
RmTerDiaPoint <- TerDiaPoint + geom_Rmark(col = "#505457") + labs(title = "geom Rmark")
CHTerDiaPoint <- TerDiaPoint + geom_crosshair_tern(col = "#deb7a0") + labs(title = "geom chrosshair tern")
#内容確認
grid.arrange(TmTerDiaPoint, LmTerDiaPoint, RmTerDiaPoint, CHTerDiaPoint)

#各頂点から線を引く:geom_Tisoprop,geom_Lisoprop,geom_Risopropコマンド
#位置を指定:valueオプション
TpTerDiaPoint <- TerDiaPoint + geom_Tisoprop(col = "#4b61ba", value = c(.4, .6)) + labs(title = "geom Tisoprop")
LpTerDiaPoint <- TerDiaPoint + geom_Lisoprop(col = "#a87963", value = c(.4, .6)) + labs(title = "geom Lisoprop")
RpTerDiaPoint <- TerDiaPoint + geom_Risoprop(col = "#505457", value = c(.4, .6)) + labs(title = "geom Risoprop")
TLRTerDiaPoint <- TerDiaPoint + geom_Tisoprop(col = "#4b61ba", value = c(.4, .6)) +
  geom_Risoprop(col = "#505457", value = c(.4, .6)) +
  geom_Lisoprop(col = "#a87963", value = c(.4, .6)) +
  labs(title = "ALL isoprop")
#内容確認
grid.arrange(TpTerDiaPoint, LpTerDiaPoint, RpTerDiaPoint, TLRTerDiaPoint)

